﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBits
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
            // Set fonts
            helpTabControl.Font = aboutTabPage.Font = aboutRichTextBox.Font = addTabPage.Font = addRichTextBox.Font = licenseTabPage.Font = licenseRichTextBox.Font = FontLoader.GetSpecialFont(10);
        }
    }
}
