﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBits
{
    [Serializable()]
    class Comic
    {
        public string address;
        public string addressRoot;
        public string name;

        public Comic(string name, string address, string addressRoot)
        {
            this.address = address;
            this.addressRoot = addressRoot;
            this.name = name;
        }

        public static int Comparison(Comic a, Comic b)
        {
            return a.name.CompareTo(b.name);
        }
    }
}
