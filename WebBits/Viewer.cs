﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using CefSharp;
using CefSharp.WinForms;
using CefSharp.SchemeHandler;

namespace WebBits
{
    public partial class Viewer : Form
    {
        private string saveLoc = @".\WebBits.bin";
        private List<Comic> comics = new List<Comic>();
        private int _modComic;
        private ChromiumWebBrowser chromeBrowser;
        private string title = "WebBits", pathover = "";

        public Viewer()
        {
            InitializeComponent();
            InitializeChromium();

            // Set fonts
            urlLabel.Font = comicListBox.Font = addButton.Font = helpButton.Font = backButton.Font = foreButton.Font = FontLoader.GetSpecialFont(10);

            // Load saved comics if they exist
            if (File.Exists(saveLoc))
            {
                Stream TestFileStream = File.OpenRead(saveLoc);
                BinaryFormatter deserializer = new BinaryFormatter();
                comics = (List<Comic>)deserializer.Deserialize(TestFileStream);
                TestFileStream.Close();
                updateList();
                if (comics.Count > 0)
                    comicListBox.SelectedIndex = 0;
                else
                {
                    pathover = "Welcome to WebBits";
                    chromeBrowser.Load(string.Format(@"{0}\hello.html", Application.StartupPath));
                }
            }
        }

        /// <summary>
        /// Start the Chromium framework
        /// </summary>
        private void InitializeChromium()
        {   
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            chromeBrowser = new ChromiumWebBrowser(null);
            chromePanel.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
            chromeBrowser.AddressChanged += ChromeBrowser_AddressChanged;
            chromeBrowser.FrameLoadEnd += ChromeBrowser_FrameLoadEnd;
        }
        
        // Void for delegation
        private delegate void DelegateVoid();

        private void ChromeBrowser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            Invoke(new DelegateVoid(FrameLoadEnd), new object[] { });
        }

        private void FrameLoadEnd()
        {
            Text = title;
        }

        private void ChromeBrowser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            Invoke(new DelegateVoid(AddressChange), new object[] { });
        }

        private void AddressChange()
        {
            Text = title + " - Loading";

            if (pathover == "")
                urlLabel.Text = chromeBrowser.Address;
            else
                urlLabel.Text = pathover;

            int i = comicListBox.SelectedIndex;
            if (i >= 0 && i < comics.Count)
                // Update url if the same root pattern exists
                if (chromeBrowser.Address.Contains(comics.ElementAt(i).addressRoot))
                    comics.ElementAt(i).address = chromeBrowser.Address;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            (new AddComic(this)).ShowDialog();
        }

        public void returnNewComic(string name, string url, string urlRoot)
        {
            comics.Add(new Comic(name, url, urlRoot));
            updateList();
        }

        public void returnModComic(string name, string url, string urlRoot)
        {
            comics.ElementAt(_modComic).name = name;
            comics.ElementAt(_modComic).address = url;
            comics.ElementAt(_modComic).addressRoot = urlRoot;
            updateList();
        }

        private void updateList()
        {
            comics.Sort(Comic.Comparison);

            comicListBox.Items.Clear();

            foreach (Comic comic in comics)
                comicListBox.Items.Add(comic.name);
        }

        private void comicListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            pathover = "";
            int i = comicListBox.SelectedIndex;
            if (i >= 0 && i < comics.Count)
            {
                Uri u;
                if (Uri.TryCreate(comics.ElementAt(i).address, UriKind.Absolute, out u) && (u.Scheme == Uri.UriSchemeHttp || u.Scheme == Uri.UriSchemeHttps))
                    chromeBrowser.Load(comics.ElementAt(i).address);
                else
                {
                    pathover = "Page Not Found";
                    chromeBrowser.Load(string.Format(@"{0}\pageNotFound.html", Application.StartupPath));
                }
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            if (chromeBrowser.CanGoBack)
                chromeBrowser.Back();
        }

        private void foreButton_Click(object sender, EventArgs e)
        {
            if (chromeBrowser.CanGoForward)
                chromeBrowser.Forward();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stream saveFile = File.Create(saveLoc);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(saveFile, comics);
            saveFile.Close();
            Cef.Shutdown();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = comicListBox.SelectedIndex;
            if (i >= 0 && i < comics.Count)
            {
                _modComic = i;
                (new AddComic(this, comics.ElementAt(i).name, comics.ElementAt(i).address, comics.ElementAt(i).addressRoot)).ShowDialog();
            }
            else
                MessageBox.Show("You need to select a comic to do that.", "No Comic Selected");
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = comicListBox.SelectedIndex;
            if (i >= 0 && i < comics.Count)
            {
                comics.RemoveAt(comicListBox.SelectedIndex);
                updateList();
            }
            else
                MessageBox.Show("You need to select a comic to do that.", "No Comic Selected");
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            new Help().ShowDialog();
        }

        private void Viewer_Resize(object sender, EventArgs e)
        {
            urlLabel_TextChanged(sender, e);
        }

        /// <summary>
        /// Resize text field on text change
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">Not used</param>
        private void urlLabel_TextChanged(object sender, EventArgs e)
        {
           urlLabel.Width = Math.Min(TextRenderer.MeasureText(urlLabel.Text, urlLabel.Font).Width, browserControlTableLayoutPanel.GetColumnWidths()[0] - 10);
        }
    }
}
