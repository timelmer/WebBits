﻿namespace WebBits
{
    partial class Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Viewer));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.comicsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.comicListBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addButton = new System.Windows.Forms.Button();
            this.browserTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.chromePanel = new System.Windows.Forms.Panel();
            this.browserControlTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.browserButtonPanel = new System.Windows.Forms.Panel();
            this.foreButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.urlLabel = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.comicsTableLayoutPanel.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.browserTableLayoutPanel.SuspendLayout();
            this.browserControlTableLayoutPanel.SuspendLayout();
            this.browserButtonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.comicsTableLayoutPanel);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.browserTableLayoutPanel);
            this.splitContainer.Size = new System.Drawing.Size(784, 461);
            this.splitContainer.SplitterDistance = 220;
            this.splitContainer.TabIndex = 0;
            // 
            // comicsTableLayoutPanel
            // 
            this.comicsTableLayoutPanel.ColumnCount = 1;
            this.comicsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.comicsTableLayoutPanel.Controls.Add(this.comicListBox, 0, 1);
            this.comicsTableLayoutPanel.Controls.Add(this.addButton, 0, 0);
            this.comicsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comicsTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.comicsTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.comicsTableLayoutPanel.Name = "comicsTableLayoutPanel";
            this.comicsTableLayoutPanel.RowCount = 2;
            this.comicsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.comicsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.comicsTableLayoutPanel.Size = new System.Drawing.Size(220, 461);
            this.comicsTableLayoutPanel.TabIndex = 0;
            // 
            // comicListBox
            // 
            this.comicListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.comicListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.comicListBox.ContextMenuStrip = this.contextMenuStrip;
            this.comicListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comicListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comicListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.comicListBox.FormattingEnabled = true;
            this.comicListBox.HorizontalScrollbar = true;
            this.comicListBox.ItemHeight = 16;
            this.comicListBox.Location = new System.Drawing.Point(3, 37);
            this.comicListBox.Name = "comicListBox";
            this.comicListBox.Size = new System.Drawing.Size(214, 426);
            this.comicListBox.TabIndex = 0;
            this.comicListBox.SelectedValueChanged += new System.EventHandler(this.comicListBox_SelectedValueChanged);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(118, 48);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // addButton
            // 
            this.addButton.AutoSize = true;
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(154)))), ((int)(((byte)(143)))));
            this.addButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(199)))), ((int)(((byte)(185)))));
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.addButton.Location = new System.Drawing.Point(3, 3);
            this.addButton.MaximumSize = new System.Drawing.Size(175, 50);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(86, 28);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Add Comic";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // browserTableLayoutPanel
            // 
            this.browserTableLayoutPanel.ColumnCount = 1;
            this.browserTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.browserTableLayoutPanel.Controls.Add(this.chromePanel, 0, 1);
            this.browserTableLayoutPanel.Controls.Add(this.browserControlTableLayoutPanel, 0, 0);
            this.browserTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.browserTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.browserTableLayoutPanel.Name = "browserTableLayoutPanel";
            this.browserTableLayoutPanel.RowCount = 2;
            this.browserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.browserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.browserTableLayoutPanel.Size = new System.Drawing.Size(560, 461);
            this.browserTableLayoutPanel.TabIndex = 0;
            // 
            // chromePanel
            // 
            this.chromePanel.AutoSize = true;
            this.chromePanel.BackColor = System.Drawing.Color.Transparent;
            this.chromePanel.Cursor = System.Windows.Forms.Cursors.No;
            this.chromePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chromePanel.Location = new System.Drawing.Point(0, 34);
            this.chromePanel.Margin = new System.Windows.Forms.Padding(0);
            this.chromePanel.Name = "chromePanel";
            this.chromePanel.Size = new System.Drawing.Size(560, 427);
            this.chromePanel.TabIndex = 4;
            // 
            // browserControlTableLayoutPanel
            // 
            this.browserControlTableLayoutPanel.AutoSize = true;
            this.browserControlTableLayoutPanel.ColumnCount = 2;
            this.browserControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.browserControlTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.browserControlTableLayoutPanel.Controls.Add(this.browserButtonPanel, 1, 0);
            this.browserControlTableLayoutPanel.Controls.Add(this.urlLabel, 0, 0);
            this.browserControlTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.browserControlTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.browserControlTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.browserControlTableLayoutPanel.Name = "browserControlTableLayoutPanel";
            this.browserControlTableLayoutPanel.RowCount = 1;
            this.browserControlTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.browserControlTableLayoutPanel.Size = new System.Drawing.Size(560, 34);
            this.browserControlTableLayoutPanel.TabIndex = 5;
            // 
            // browserButtonPanel
            // 
            this.browserButtonPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browserButtonPanel.AutoSize = true;
            this.browserButtonPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.browserButtonPanel.Controls.Add(this.foreButton);
            this.browserButtonPanel.Controls.Add(this.backButton);
            this.browserButtonPanel.Controls.Add(this.helpButton);
            this.browserButtonPanel.Location = new System.Drawing.Point(314, 0);
            this.browserButtonPanel.Margin = new System.Windows.Forms.Padding(0);
            this.browserButtonPanel.Name = "browserButtonPanel";
            this.browserButtonPanel.Size = new System.Drawing.Size(246, 34);
            this.browserButtonPanel.TabIndex = 5;
            // 
            // foreButton
            // 
            this.foreButton.AutoSize = true;
            this.foreButton.Cursor = System.Windows.Forms.Cursors.PanEast;
            this.foreButton.FlatAppearance.BorderSize = 0;
            this.foreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(154)))), ((int)(((byte)(143)))));
            this.foreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(199)))), ((int)(((byte)(185)))));
            this.foreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.foreButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.foreButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.foreButton.Location = new System.Drawing.Point(168, 3);
            this.foreButton.Name = "foreButton";
            this.foreButton.Size = new System.Drawing.Size(75, 28);
            this.foreButton.TabIndex = 5;
            this.foreButton.Text = "Forward";
            this.foreButton.UseVisualStyleBackColor = true;
            this.foreButton.Click += new System.EventHandler(this.foreButton_Click);
            // 
            // backButton
            // 
            this.backButton.AutoSize = true;
            this.backButton.Cursor = System.Windows.Forms.Cursors.PanWest;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(154)))), ((int)(((byte)(143)))));
            this.backButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(199)))), ((int)(((byte)(185)))));
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.backButton.Location = new System.Drawing.Point(87, 3);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 26);
            this.backButton.TabIndex = 4;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // helpButton
            // 
            this.helpButton.AutoSize = true;
            this.helpButton.Cursor = System.Windows.Forms.Cursors.Help;
            this.helpButton.FlatAppearance.BorderSize = 0;
            this.helpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(154)))), ((int)(((byte)(143)))));
            this.helpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(199)))), ((int)(((byte)(185)))));
            this.helpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.helpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.helpButton.Location = new System.Drawing.Point(6, 3);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(75, 26);
            this.helpButton.TabIndex = 6;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = false;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // urlLabel
            // 
            this.urlLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.urlLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.urlLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.urlLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urlLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.urlLabel.Location = new System.Drawing.Point(3, 9);
            this.urlLabel.Name = "urlLabel";
            this.urlLabel.ReadOnly = true;
            this.urlLabel.Size = new System.Drawing.Size(100, 15);
            this.urlLabel.TabIndex = 2;
            this.urlLabel.Text = "Loading";
            this.urlLabel.TextChanged += new System.EventHandler(this.urlLabel_TextChanged);
            // 
            // Viewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Viewer";
            this.Text = "WebBits";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Resize += new System.EventHandler(this.Viewer_Resize);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.comicsTableLayoutPanel.ResumeLayout(false);
            this.comicsTableLayoutPanel.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.browserTableLayoutPanel.ResumeLayout(false);
            this.browserTableLayoutPanel.PerformLayout();
            this.browserControlTableLayoutPanel.ResumeLayout(false);
            this.browserControlTableLayoutPanel.PerformLayout();
            this.browserButtonPanel.ResumeLayout(false);
            this.browserButtonPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TableLayoutPanel browserTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel comicsTableLayoutPanel;
        private System.Windows.Forms.ListBox comicListBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.Panel chromePanel;
        private System.Windows.Forms.Panel browserButtonPanel;
        private System.Windows.Forms.Button foreButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.TextBox urlLabel;
        private System.Windows.Forms.TableLayoutPanel browserControlTableLayoutPanel;
    }
}

