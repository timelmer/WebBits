﻿namespace WebBits
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.helpTabControl = new System.Windows.Forms.TabControl();
            this.aboutTabPage = new System.Windows.Forms.TabPage();
            this.aboutRichTextBox = new System.Windows.Forms.RichTextBox();
            this.addTabPage = new System.Windows.Forms.TabPage();
            this.addRichTextBox = new System.Windows.Forms.RichTextBox();
            this.licenseTabPage = new System.Windows.Forms.TabPage();
            this.licenseRichTextBox = new System.Windows.Forms.RichTextBox();
            this.helpTabControl.SuspendLayout();
            this.aboutTabPage.SuspendLayout();
            this.addTabPage.SuspendLayout();
            this.licenseTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpTabControl
            // 
            this.helpTabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.helpTabControl.Controls.Add(this.aboutTabPage);
            this.helpTabControl.Controls.Add(this.addTabPage);
            this.helpTabControl.Controls.Add(this.licenseTabPage);
            this.helpTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpTabControl.Location = new System.Drawing.Point(0, 0);
            this.helpTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.helpTabControl.Multiline = true;
            this.helpTabControl.Name = "helpTabControl";
            this.helpTabControl.SelectedIndex = 0;
            this.helpTabControl.Size = new System.Drawing.Size(704, 441);
            this.helpTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.helpTabControl.TabIndex = 0;
            // 
            // aboutTabPage
            // 
            this.aboutTabPage.BackColor = System.Drawing.Color.Transparent;
            this.aboutTabPage.Controls.Add(this.aboutRichTextBox);
            this.aboutTabPage.Location = new System.Drawing.Point(23, 4);
            this.aboutTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.aboutTabPage.Name = "aboutTabPage";
            this.aboutTabPage.Size = new System.Drawing.Size(677, 433);
            this.aboutTabPage.TabIndex = 1;
            this.aboutTabPage.Text = "About";
            // 
            // aboutRichTextBox
            // 
            this.aboutRichTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.aboutRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.aboutRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aboutRichTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.aboutRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.aboutRichTextBox.Name = "aboutRichTextBox";
            this.aboutRichTextBox.ReadOnly = true;
            this.aboutRichTextBox.Size = new System.Drawing.Size(677, 433);
            this.aboutRichTextBox.TabIndex = 0;
            this.aboutRichTextBox.Text = resources.GetString("aboutRichTextBox.Text");
            // 
            // addTabPage
            // 
            this.addTabPage.Controls.Add(this.addRichTextBox);
            this.addTabPage.Location = new System.Drawing.Point(23, 4);
            this.addTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.addTabPage.Name = "addTabPage";
            this.addTabPage.Size = new System.Drawing.Size(677, 433);
            this.addTabPage.TabIndex = 3;
            this.addTabPage.Text = "Add/Modify Comic";
            this.addTabPage.UseVisualStyleBackColor = true;
            // 
            // addRichTextBox
            // 
            this.addRichTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.addRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addRichTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.addRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.addRichTextBox.Name = "addRichTextBox";
            this.addRichTextBox.ReadOnly = true;
            this.addRichTextBox.Size = new System.Drawing.Size(677, 433);
            this.addRichTextBox.TabIndex = 0;
            this.addRichTextBox.Text = resources.GetString("addRichTextBox.Text");
            // 
            // licenseTabPage
            // 
            this.licenseTabPage.Controls.Add(this.licenseRichTextBox);
            this.licenseTabPage.Location = new System.Drawing.Point(23, 4);
            this.licenseTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.licenseTabPage.Name = "licenseTabPage";
            this.licenseTabPage.Size = new System.Drawing.Size(677, 433);
            this.licenseTabPage.TabIndex = 2;
            this.licenseTabPage.Text = "License";
            this.licenseTabPage.UseVisualStyleBackColor = true;
            // 
            // licenseRichTextBox
            // 
            this.licenseRichTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.licenseRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.licenseRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.licenseRichTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.licenseRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.licenseRichTextBox.Name = "licenseRichTextBox";
            this.licenseRichTextBox.ReadOnly = true;
            this.licenseRichTextBox.Size = new System.Drawing.Size(677, 433);
            this.licenseRichTextBox.TabIndex = 0;
            this.licenseRichTextBox.Text = resources.GetString("licenseRichTextBox.Text");
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(124)))), ((int)(((byte)(132)))));
            this.ClientSize = new System.Drawing.Size(704, 441);
            this.Controls.Add(this.helpTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Help";
            this.ShowInTaskbar = false;
            this.Text = "Help";
            this.helpTabControl.ResumeLayout(false);
            this.aboutTabPage.ResumeLayout(false);
            this.addTabPage.ResumeLayout(false);
            this.licenseTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl helpTabControl;
        private System.Windows.Forms.TabPage aboutTabPage;
        private System.Windows.Forms.RichTextBox aboutRichTextBox;
        private System.Windows.Forms.TabPage licenseTabPage;
        private System.Windows.Forms.RichTextBox licenseRichTextBox;
        private System.Windows.Forms.TabPage addTabPage;
        private System.Windows.Forms.RichTextBox addRichTextBox;
    }
}