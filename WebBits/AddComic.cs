﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBits
{
    public partial class AddComic : Form
    {
        private Viewer parent;
        private bool edit = false;

        public AddComic(Viewer parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        public AddComic(Viewer parent, string name, string url, string urlRoot)
        {
            InitializeComponent();
            this.parent = parent;
            nameTextBox.Text = name;
            urlTextBox.Text = url;
            urlRootTextBox.Text = urlRoot;
            edit = true;
            addButton.Text = "Save";
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text.Length <= 0 || urlTextBox.Text.Length <= 0 || urlRootTextBox.Text.Length <= 0)
            {
                MessageBox.Show("All fields must be filled.");
                return;
            }

            if (!edit)
                parent.returnNewComic(nameTextBox.Text, urlTextBox.Text, urlRootTextBox.Text);
            else
                parent.returnModComic(nameTextBox.Text, urlTextBox.Text, urlRootTextBox.Text);
            cancelButton.PerformClick();
        }
    }
}
